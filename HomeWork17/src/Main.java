import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        int [] numbers = new int[new Random().nextInt(15000) + 3000];
        int sum1 = 0, sum2 = 0, sum3 = 0;

        Arrays.fill(numbers, 1);

        long before = System.currentTimeMillis();

        CalcThread calcThread1 = new CalcThread(numbers, 0, numbers.length / 3);
        CalcThread calcThread2 = new CalcThread(numbers, numbers.length / 3, numbers.length / 3 * 2);
        CalcThread calcThread3 = new CalcThread(numbers, numbers.length / 3 * 2, numbers.length);

        calcThread1.start();
        calcThread2.start();
        calcThread3.start();

        try {
            calcThread1.join();
            calcThread2.join();
            calcThread3.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        System.out.println("Sum by threads: " + (calcThread1.getSum() + calcThread2.getSum() + calcThread3.getSum()));

        long after = System.currentTimeMillis();
        System.out.println("Execution time (3 threads): " + (after - before) / 1000 + " sec.\n");

        before = System.currentTimeMillis();

        for (int i = 0; i < numbers.length / 3; i++) {
            try {
                sum1 += numbers[i];
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        System.out.print(Thread.currentThread().getName() + ": " + sum1);

        for (int i = numbers.length / 3; i < numbers.length / 3 * 2; i++) {
            try {
                sum2 += numbers[i];
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        System.out.print(" + " + sum2);

        for (int i = numbers.length / 3 * 2; i < numbers.length; i++) {
            try {
                sum3 += numbers[i];
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        System.out.print(" + " + sum3);

        System.out.println(" = " + (sum1 + sum2 + sum3));

        after = System.currentTimeMillis();
        System.out.println("Execution time (1 thread): " + (after - before) / 1000 + " sec.");
    }
}
