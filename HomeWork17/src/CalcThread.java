public class CalcThread extends Thread {
    private int[] numbers;
    private int startIndex;
    private int endIndex;

    private int sum = 0;

    public CalcThread(int[] numbers, int startIndex, int endIndex) {
        this.numbers = numbers;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Override
    public void run() {
        for (int i = startIndex; i < endIndex; i++) {
            try {
                sum += numbers[i];
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        System.out.println(Thread.currentThread().getName() + ": from " + startIndex + " to " + (endIndex - 1) + " sum is " + sum);
    }

    public int getSum() {
        return sum;
    }
}
