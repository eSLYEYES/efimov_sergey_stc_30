package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class PlayersRepositoryImpl implements PlayersRepository {

    private static final String SQL_INSERT = "insert into player (last_ip, name, max_points, win_number, los_number) " +
            "values (?, ?, ?, ?, ?);";

    private static final String SQL_FIND_BY_ID = "select * from player where id = ?";

    private static final String SQL_FIND_BY_NAME = "select * from player where name = ?";

    private static final String SQL_UPDATE = "update player set " +
            "last_ip = ?, " +
            "name = ?, " +
            "max_points = ?, " +
            "win_number = ?, " +
            "los_number = ? " +
            "where id = ?";

    private DataSource dataSource;

    public PlayersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Player findByName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME)) {

            statement.setString(1, name);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Player.builder()
                        .id(result.getInt("id"))
                        .lastIp(result.getString("last_ip"))
                        .name(result.getString("name"))
                        .maxPoints(result.getInt("max_points"))
                        .winNumber(result.getInt("win_number"))
                        .losNumber(result.getInt("los_number"))
                        .build();
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Player findById(Integer id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Player.builder()
                        .id(result.getInt("id"))
                        .lastIp(result.getString("last_ip"))
                        .name(result.getString("name"))
                        .maxPoints(result.getInt("max_points"))
                        .winNumber(result.getInt("win_number"))
                        .losNumber(result.getInt("los_number"))
                        .build();
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void add(Player player) {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getLastIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getMaxPoints());
            statement.setInt(4, player.getWinNumber());
            statement.setInt(5, player.getLosNumber());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }

            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id");
                player.setId(generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, player.getLastIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getMaxPoints());
            statement.setInt(4, player.getWinNumber());
            statement.setInt(5, player.getLosNumber());
            statement.setInt(6, player.getId());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
