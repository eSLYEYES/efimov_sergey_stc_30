package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

public interface ShotsRepository {
    void add(Shot shot);
}
