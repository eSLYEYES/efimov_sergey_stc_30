package ru.inno.sockets.services;

import ru.inno.sockets.models.Game;

public interface TanksService {
//    // срабатывает при входе пользователя в игру
//    // если пользователь с таким никнеймом уже есть, то мы используем его
//    // если пользователя с таким никнеймом еще нет - то создаем его
//    Player createOrUpdatePlayer(Player player);
//
//    // срабатываем при начале
//    Game startGame(Game game);
//
//    // срабатывает при завершении игры
//    void finishGameForPlayers(Game game);
//

    Integer startGame(String firstPlayerNickname, String firstPlayerIp, String secondPlayerNickname, String secondPlayerIp);
    void shot(Integer gameId, String shooterNickname, String targetNickname);
    void finishGame(Integer gameId, String winner, Integer firstPlayerScore, Integer secondPlayerScore);
}
