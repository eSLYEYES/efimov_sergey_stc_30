package ru.inno.sockets;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.sockets.repositories.*;
import ru.inno.sockets.server.GameServer;
import ru.inno.sockets.services.TanksServiceImpl;

public class Main {
    public static void main(String[] args) {

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/stc_30_final");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("derrik");
        hikariConfig.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        GamesRepository gamesRepository = new GamesRepositoryImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryImpl(dataSource);
        GameServer gameServer = new GameServer(new TanksServiceImpl(shotsRepository, playersRepository, gamesRepository));
        gameServer.start(7777);

    }
}
