package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

public interface PlayersRepository {

    void add(Player player);
    void update(Player player);
    Player findByName(String name);
    Player findById(Integer id);
}
