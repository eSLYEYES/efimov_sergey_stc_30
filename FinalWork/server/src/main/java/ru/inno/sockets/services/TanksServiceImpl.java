package ru.inno.sockets.services;

import ru.inno.sockets.models.Game;
import ru.inno.sockets.models.Player;
import ru.inno.sockets.models.Shot;
import ru.inno.sockets.repositories.*;

import java.sql.Date;

public class TanksServiceImpl implements TanksService {

    private ShotsRepository shotsRepository;
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;

    long startGame;

    public TanksServiceImpl(ShotsRepository shotsRepository, PlayersRepository playersRepository, GamesRepository gamesRepository) {
        this.shotsRepository = shotsRepository;
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
    }

    public Integer startGame(String firstPlayerNickname, String firstPlayerIp, String secondPlayerNickname, String secondPlayerIp) {
        Player firstPlayer = playersRepository.findByName(firstPlayerNickname);
        Player secondPlayer = playersRepository.findByName(secondPlayerNickname);
        // если пользователи новые
        if (firstPlayer == null) {
            firstPlayer = new Player(firstPlayerNickname);
            firstPlayer.setLastIp(firstPlayerIp);
            playersRepository.add(firstPlayer);
        } else {
            firstPlayer.setLastIp(firstPlayerIp);
            playersRepository.update(firstPlayer);
        }

        if (secondPlayer == null) {
            secondPlayer = new Player(secondPlayerNickname);
            secondPlayer.setLastIp(secondPlayerIp);
            playersRepository.add(secondPlayer);
        }else {
            secondPlayer.setLastIp(secondPlayerIp);
            playersRepository.update(secondPlayer);
        }

        Game game = new Game();
        game.setPlayerOne(firstPlayer);
        game.setPlayerTwo(secondPlayer);
        game.setDate(new Date(System.currentTimeMillis()));
        startGame = System.currentTimeMillis();
        gamesRepository.add(game);
        return game.getId();
    }

    public void shot(Integer gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByName(shooterNickname);
        Player target = playersRepository.findByName(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(game, shooter, target);
        if (game.getPlayerOne().equals(shooter)) {
            game.setShotCountPlayerOne(game.getShotCountPlayerOne() + 1);
        } else if (game.getPlayerTwo().equals(shooter)) {
            game.setShotCountPlayerTwo(game.getShotCountPlayerTwo() + 1);
        }
        shotsRepository.add(shot);
        gamesRepository.update(game);
        System.out.println(" shots: " +
                game.getShotCountPlayerOne() + "|" + game.getShotCountPlayerTwo());
    }

    @Override
    public void finishGame(Integer gameId, String winner, Integer firstPlayerScore, Integer secondPlayerScore) {
        Game game = gamesRepository.findById(gameId);
        Player playerOne = playersRepository.findByName(game.getPlayerOne().getName());
        Player playerTwo = playersRepository.findByName(game.getPlayerTwo().getName());
        game.setDuration((System.currentTimeMillis() - startGame) / 1000);

        if (playerOne.getName().equals(winner)){
            playerOne.setWinNumber(playerOne.getWinNumber() + 1);
            playerTwo.setLosNumber(playerTwo.getLosNumber() + 1);
        } else {
            playerTwo.setWinNumber(playerTwo.getWinNumber() + 1);
            playerOne.setLosNumber(playerOne.getLosNumber() + 1);
        }

        if (playerOne.getMaxPoints() < firstPlayerScore){
            playerOne.setMaxPoints(firstPlayerScore);
        }

        if (playerTwo.getMaxPoints() < secondPlayerScore){
            playerTwo.setMaxPoints(secondPlayerScore);
        }

        gamesRepository.update(game);
        playersRepository.update(playerOne);
        playersRepository.update(playerTwo);
    }
}
