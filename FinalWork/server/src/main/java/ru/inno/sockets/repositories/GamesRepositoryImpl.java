package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;
import ru.inno.sockets.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class GamesRepositoryImpl implements GamesRepository {

    private static final String SQL_INSERT = "insert into game (date, player_one, player_two, shot_count_player_one, shot_count_player_two, game_duration) values (?, ?, ?, ?, ?, ?);";

    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";

    private static final String SQL_UPDATE = "update game set " +
            "date = ?, " +
            "player_one = ?, " +
            "player_two = ?, " +
            "shot_count_player_one = ?, " +
            "shot_count_player_two = ?, " +
            "game_duration = ? " +
            "where id = ?";

    private DataSource dataSource;

    public GamesRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void add(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setDate(1, game.getDate());
            statement.setInt(2, game.getPlayerOne().getId());
            statement.setInt(3, game.getPlayerTwo().getId());
            statement.setInt(4, game.getShotCountPlayerOne());
            statement.setInt(5, game.getShotCountPlayerTwo());
            statement.setLong(6, game.getDuration());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }

            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id");
                game.setId(generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Integer gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, gameId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                PlayersRepository playersRepository = new PlayersRepositoryImpl(dataSource);
                Player playerOne = playersRepository.findById(result.getInt("player_one"));
                Player playerTwo = playersRepository.findById(result.getInt("player_two"));

                return Game.builder()
                        .id(result.getInt("id"))
                        .date(result.getDate("date"))
                        .playerOne(playerOne)
                        .playerTwo(playerTwo)
                        .shotCountPlayerOne(result.getInt("shot_count_player_one"))
                        .shotCountPlayerTwo(result.getInt("shot_count_player_two"))
                        .duration((long)result.getInt("game_duration"))
                        .build();
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setDate(1, game.getDate());
            statement.setInt(2, game.getPlayerOne().getId());
            statement.setInt(3, game.getPlayerTwo().getId());
            statement.setInt(4, game.getShotCountPlayerOne());
            statement.setInt(5, game.getShotCountPlayerTwo());
            statement.setLong(6, game.getDuration());
            statement.setInt(7, game.getId());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
