package ru.inno.sockets.models;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Shot {

    private Long id;
    private Timestamp shotTime;
    private boolean hit;
    private Game game;
    private Player whoShot;
    private Player whoWasShot;

    public Shot(Game game, Player whoShot, Player whoWasShot) {
        this.shotTime = new Timestamp(System.currentTimeMillis());
        this.hit = false;
        this.game = game;
        this.whoShot = whoShot;
        this.whoWasShot = whoWasShot;
    }
}
