package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor

public class Game {
    private Integer id;
    private java.sql.Date date;
    private Player playerOne;
    private Player playerTwo;
    private Integer shotCountPlayerOne = 0;
    private Integer shotCountPlayerTwo = 0;
    private Long duration = 0L;

    public Game() {
    }
}
