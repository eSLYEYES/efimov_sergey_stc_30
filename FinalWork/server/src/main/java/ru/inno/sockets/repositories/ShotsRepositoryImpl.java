package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryImpl implements ShotsRepository {

    private static final String SQL_INSERT = "insert into shot (shot_time, hit, game_id, who_shot, who_was_shot) values (?, ?, ?, ?, ?);";

    private DataSource dataSource;

    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void add(Shot shot) {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp(1, shot.getShotTime());
            statement.setBoolean(2, shot.isHit());
            statement.setInt(3, shot.getGame().getId());
            statement.setInt(4, shot.getWhoShot().getId());
            statement.setInt(5, shot.getWhoWasShot().getId());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity (shot)");
            }

            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                Long generatedId = generatedIds.getLong("id");
                shot.setId(generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();

            System.out.println("ShootRepository - saved " + shot.getWhoShot().getName() + " "
                    + shot.getWhoWasShot().getName() + " with game id = " + shot.getGame().getId());

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
