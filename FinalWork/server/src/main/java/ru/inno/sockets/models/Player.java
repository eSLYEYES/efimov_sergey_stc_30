package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Player {

    private Integer id;
    private String lastIp;
    private String name;
    private Integer maxPoints = 0;
    private Integer winNumber = 0;
    private Integer losNumber = 0;

    public Player(String name) {
        this.name = name;
    }
}
