package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;

public interface GamesRepository {

    void add(Game game);
    void update(Game game);
    Game findById(Integer gameId);
}
