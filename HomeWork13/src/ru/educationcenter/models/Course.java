package ru.educationcenter.models;

import java.util.Arrays;
import java.util.Objects;

public class Course {
    private Long id;
    private String name;
    private String startDate;
    private String endDate;
    private String teachers;
    private String lessons;

    public Course(Long id, String name, String startDate, String endDate, String teachers, String lessons) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teachers = teachers;
        this.lessons = lessons;
    }

    public Course(String name, String startDate, String endDate, String teachers, String lessons) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teachers = teachers;
        this.lessons = lessons;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTeachers() {
        return teachers;
    }

    public void setTeachers(String teachers) {
        this.teachers = teachers;
    }

    public String getLessons() {
        return lessons;
    }

    public void setLessons(String lessons) {
        this.lessons = lessons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(id, course.id) && Objects.equals(name, course.name) && Objects.equals(startDate, course.startDate) && Objects.equals(endDate, course.endDate) && Objects.equals(teachers, course.teachers) && Objects.equals(lessons, course.lessons);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, startDate, endDate, teachers, lessons);
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", teachers='" + teachers + '\'' +
                ", lessons='" + lessons + '\'' +
                '}';
    }
}
