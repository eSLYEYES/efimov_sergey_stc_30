package ru.educationcenter.models;

import java.util.Arrays;
import java.util.Objects;

public class Lesson {
    private Long id;
    private String dateTime;
    private String name;
    private String courses;

    public Lesson(Long id, String dateTime, String name, String courses) {
        this.id = id;
        this.dateTime = dateTime;
        this.name = name;
        this.courses = courses;
    }

    public Lesson(String dateTime, String name, String courses) {
        this.dateTime = dateTime;
        this.name = name;
        this.courses = courses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourses() {
        return courses;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return Objects.equals(id, lesson.id) && Objects.equals(dateTime, lesson.dateTime) && Objects.equals(name, lesson.name) && Objects.equals(courses, lesson.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateTime, name, courses);
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", dateTime='" + dateTime + '\'' +
                ", name='" + name + '\'' +
                ", courses='" + courses + '\'' +
                '}';
    }
}
