package ru.educationcenter;

import ru.educationcenter.dao.*;
import ru.educationcenter.models.Course;
import ru.educationcenter.models.Lesson;
import ru.educationcenter.models.Teacher;
import ru.educationcenter.utils.IdGeneratorFileBasedImpl;

import java.util.Arrays;

public class Main {

    private static Long[] toLongArray(String string) {
        String[] data = string.split(",");
        Long[] result = new Long[data.length];
        for (int i = 0; i < data.length; i++) {
            result[i] = Long.valueOf(data[i]);
        }
        return result;
    }

    public static void main(String[] args) {

        CoursesDao courseDao = new CoursesDaoFileBasedImpl("courses.txt", new IdGeneratorFileBasedImpl("courses_seq.txt"));
        TeachersDao teacherDao = new TeachersDaoFileBasedImpl("teachers.txt", new IdGeneratorFileBasedImpl("teachers_seq.txt"));
        LessonsDao lessonsDao = new LessonsDaoFileBasedImpl("lessons.txt", new IdGeneratorFileBasedImpl("lessons_seq.txt"));

//------Course------------------------------------------------------------------------
        //Course courseNew = new Course("Test", "12/12/2020", "20/12/2020", "1", "0,1,3");
        //courseDao.save(courseNew);

        courseDao.findById(3L).ifPresent(course -> {
            System.out.print(course.getId() + " " + course.getName());
            System.out.println("\nПреподаватели: ");
            Long[] teachers = toLongArray(course.getTeachers());
            for (int i = 0; i < teachers.length; i++) {
                teacherDao.findById(teachers[i]).ifPresent(teacher -> System.out.println("   " + teacher.getLastName() + " " + teacher.getFirstName()));
            }
            System.out.println("Уроки: ");
            Long[] lessons = toLongArray(course.getLessons());
            for (int i = 0; i < lessons.length; i++) {
                lessonsDao.findById(lessons[i]).ifPresent(lesson -> System.out.println("   " + lesson.getDateTime() + " " + lesson.getName()));
            }

        });

        System.out.println(courseDao.findByName("OraclePM"));
        System.out.println(courseDao.findByName("OraclePM11"));

        System.out.println(courseDao.findAll());

        System.out.println();

//------Teacher------------------------------------------------------------------------------
        teacherDao.findById(4L).ifPresent(teacher -> {
            System.out.print(teacher.getId() + " " + teacher.getLastName() + ", стаж " + teacher.getExperience() + " -> ");
            System.out.println("Курсы: ");
            Long[] data = toLongArray(teacher.getCourses());
            for (int i = 0; i < data.length; i++) {
                courseDao.findById(data[i]).ifPresent(course -> System.out.println("   " + course.getName()));
            }
        });
        System.out.println(teacherDao.findAll());

        //Teacher teacherForDelete = new Teacher(3L, "Фёдор", "Емельяненко", 10,"0,2,4,8");
        //teacherDao.delete(teacherForDelete);

        System.out.println();

//------Lesson------------------------------------------------------------------------------
        //Lesson lessonNew = new Lesson("12:00", "Lesson18", "0,3");
        //lessonsDao.save(lessonNew);

        System.out.println(lessonsDao.findByName("lesson1"));
        System.out.println(lessonsDao.findByName("Lesson1"));

        //Lesson lessonForUpdate = new Lesson(8L, "31/12/2020 23:29", "Lesson18", "1,3");
        //lessonsDao.update(lessonForUpdate);

    }
}
