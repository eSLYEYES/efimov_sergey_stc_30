package ru.educationcenter.dao;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {
    void save(T entity);
    void update (T entity);
    void delete (T entity);
    List<T> findAll();
    Optional<T> findById(Long id);
}
