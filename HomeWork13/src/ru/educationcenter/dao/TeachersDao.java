package ru.educationcenter.dao;

import ru.educationcenter.models.Teacher;

public interface TeachersDao extends CrudDao<Teacher> {
    boolean findByLastName(String lastName);
}
