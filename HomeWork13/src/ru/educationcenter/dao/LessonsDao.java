package ru.educationcenter.dao;

import ru.educationcenter.models.Lesson;

public interface LessonsDao extends CrudDao<Lesson>{
    boolean findByName(String name);
}
