package ru.educationcenter.dao;

public interface Mapper<X, Y> {
    Y map(X x);
}
