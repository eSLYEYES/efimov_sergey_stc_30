package ru.educationcenter.dao;

import ru.educationcenter.models.Course;

import java.util.Optional;

public interface CoursesDao extends CrudDao<Course>{
    boolean findByName(String name);
}
