package ru.educationcenter.dao;

import ru.educationcenter.models.Course;
import ru.educationcenter.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CoursesDaoFileBasedImpl implements CoursesDao {
    private String fileName;
    private IdGenerator idGenerator;

    private Mapper<Course, String> courseToStringMapper = course ->
                    course.getId() + ";" +
                    course.getName() + ";" +
                    course.getStartDate() + ";" +
                    course.getEndDate() + ";" +
                    course.getTeachers() + ";" +
                    course.getLessons() + "\r\n";

    private Mapper<String, Course> stringToCourseMapper = string -> {
        String[] data = string.split(";");
        return new Course(Long.parseLong(data[0]), data[1], data[2], data[3], data[4], data[5]);
    };

    public CoursesDaoFileBasedImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public boolean findByName(String name) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String[] data = current.split(";");
                if (data[1].equals(name)) {
                    return true;
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return false;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Course course) {
        try {
            course.setId(idGenerator.nextId());
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(courseToStringMapper.map(course).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Course entity) {
        if (findById(entity.getId()).isPresent()){
            try {
                List<Course> courses = new ArrayList<>();
                BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
                String current = bufferedReader.readLine();
                while (current != null) {
                    Course course = stringToCourseMapper.map(current);
                    if (course.getId().equals(entity.getId())){
                        course.setName(entity.getName());
                        course.setStartDate(entity.getStartDate());
                        course.setEndDate(entity.getEndDate());
                        course.setTeachers(entity.getTeachers());
                        course.setLessons(entity.getLessons());
                    }
                    courses.add(course);
                    current = bufferedReader.readLine();
                }
                bufferedReader.close();
                OutputStream outputStream = new FileOutputStream(fileName, false);
                for (Course course:courses) {
                    outputStream.write(courseToStringMapper.map(course).getBytes());
                }
                outputStream.close();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        } else {
            System.out.println("Курс не найден и не может быть изиенен");
        }
    }

    @Override
    public void delete(Course entity) {
        try {
            List<Course> courses = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            boolean isExist = false;
            while (current != null) {
                Course course = stringToCourseMapper.map(current);
                if (course.equals(entity)){
                    isExist = true;
                } else {
                    courses.add(course);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();

            if(isExist) {
                OutputStream outputStream = new FileOutputStream(fileName, false);
                for (Course course : courses) {
                    outputStream.write(courseToStringMapper.map(course).getBytes());
                }
                outputStream.close();
            } else {
                System.out.println("Курс не найден и не может быть удалён");
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Course> findAll() {
        try {
            List<Course> courses = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Course course = stringToCourseMapper.map(current);
                courses.add(course);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return courses;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Course> findById(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String[] data = current.split(";");

                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(id)) {
                    Course course = stringToCourseMapper.map(current);
                    return Optional.of(course);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
