package ru.educationcenter.dao;

import ru.educationcenter.models.Teacher;
import ru.educationcenter.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TeachersDaoFileBasedImpl implements TeachersDao {
    private String fileName;
    private IdGenerator idGenerator;

    private Mapper<Teacher, String> teacherToStringMapper = teacher ->
            teacher.getId() + ";" +
                    teacher.getFirstName() + ";" +
                    teacher.getLastName() + ";" +
                    teacher.getExperience() + ";" +
                    teacher.getCourses() + "\r\n";

    private Mapper<String, Teacher> stringToTeacherMapper = string -> {
        String[] data = string.split(";");
        return new Teacher(Long.parseLong(data[0]), data[1], data[2], Integer.parseInt(data[3]), data[4]);
    };

    public TeachersDaoFileBasedImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public boolean findByLastName(String lastName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String[] data = current.split(";");
                if (data[2].equals(lastName)) {
                    return true;
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return false;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }    }

    @Override
    public void save(Teacher teacher) {
        try {
            teacher.setId(idGenerator.nextId());
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(teacherToStringMapper.map(teacher).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Teacher entity) {
        if (findById(entity.getId()).isPresent()){
            try {
                List<Teacher> teachers = new ArrayList<>();
                BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
                String current = bufferedReader.readLine();
                while (current != null) {
                    Teacher teacher = stringToTeacherMapper.map(current);
                    if (teacher.getId().equals(entity.getId())){
                        teacher.setFirstName(entity.getFirstName());
                        teacher.setLastName(entity.getLastName());
                        teacher.setExperience(entity.getExperience());
                        teacher.setCourses(entity.getCourses());
                    }
                    teachers.add(teacher);
                    current = bufferedReader.readLine();
                }
                bufferedReader.close();
                OutputStream outputStream = new FileOutputStream(fileName, false);
                for (Teacher teacher:teachers) {
                    outputStream.write(teacherToStringMapper.map(teacher).getBytes());
                }
                outputStream.close();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        } else {
            System.out.println("Преподаватель не найден и не может быть изиенен");
        }
    }

    @Override
    public void delete(Teacher entity) {
        try {
            List<Teacher> teachers = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            boolean isExist = false;
            while (current != null) {
                Teacher teacher = stringToTeacherMapper.map(current);
                if (teacher.equals(entity)){
                    isExist = true;
                } else {
                    teachers.add(teacher);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();

            if(isExist) {
                OutputStream outputStream = new FileOutputStream(fileName, false);
                for (Teacher teacher : teachers) {
                    outputStream.write(teacherToStringMapper.map(teacher).getBytes());
                }
                outputStream.close();
            } else {
                System.out.println("Преподаватель не найден и не может быть удалён");
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Teacher> findAll() {
        try {
            List<Teacher> teachers = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Teacher teacher = stringToTeacherMapper.map(current);
                teachers.add(teacher);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return teachers;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Teacher> findById(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String[] data = current.split(";");

                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(id)) {
                    Teacher teacher = stringToTeacherMapper.map(current);
                    return Optional.of(teacher);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
