package ru.educationcenter.dao;

import ru.educationcenter.models.Lesson;
import ru.educationcenter.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonsDaoFileBasedImpl implements LessonsDao {
    private String fileName;
    private IdGenerator idGenerator;

    public LessonsDaoFileBasedImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Lesson, String> lessonToStringMapper = lesson ->
            lesson.getId() + ";" +
                    lesson.getDateTime() + ";" +
                    lesson.getName() + ";" +
                    lesson.getCourses() + "\r\n";

    private Mapper<String, Lesson> stringToLessonMapper = string -> {
        String[] data = string.split(";");
        return new Lesson(Long.parseLong(data[0]), data[1], data[2], data[3]);
    };

    @Override
    public boolean findByName(String name) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String[] data = current.split(";");
                if (data[2].equals(name)) {
                    return true;
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return false;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Lesson lesson) {
        try {
            lesson.setId(idGenerator.nextId());
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(lessonToStringMapper.map(lesson).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Lesson entity) {
        if (findById(entity.getId()).isPresent()){
            try {
                List<Lesson> lessons = new ArrayList<>();
                BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
                String current = bufferedReader.readLine();
                while (current != null) {
                    Lesson lesson = stringToLessonMapper.map(current);
                    if (lesson.getId().equals(entity.getId())){
                        lesson.setDateTime(entity.getDateTime());
                        lesson.setName(entity.getName());
                        lesson.setCourses(entity.getCourses());
                    }
                    lessons.add(lesson);
                    current = bufferedReader.readLine();
                }
                bufferedReader.close();
                OutputStream outputStream = new FileOutputStream(fileName, false);
                for (Lesson lesson:lessons) {
                    outputStream.write(lessonToStringMapper.map(lesson).getBytes());
                }
                outputStream.close();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        } else {
            System.out.println("Урок не найден и не может быть изиенен");
        }
    }

    @Override
    public void delete(Lesson entity) {
        try {
            List<Lesson> lessons = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            boolean isExist = false;
            while (current != null) {
                Lesson lesson = stringToLessonMapper.map(current);
                if (lesson.equals(entity)){
                    isExist = true;
                } else {
                    lessons.add(lesson);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();

            if(isExist) {
                OutputStream outputStream = new FileOutputStream(fileName, false);
                for (Lesson lesson : lessons) {
                    outputStream.write(lessonToStringMapper.map(lesson).getBytes());
                }
                outputStream.close();
            } else {
                System.out.println("Урок не найден и не может быть удалён");
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Lesson> findAll() {
        try {
            List<Lesson> lessons = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Lesson lesson = stringToLessonMapper.map(current);
                lessons.add(lesson);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return lessons;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public Optional<Lesson> findById(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String[] data = current.split(";");

                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(id)) {
                    Lesson lesson = stringToLessonMapper.map(current);
                    return Optional.of(lesson);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
