package ru.educationcenter.utils;

public interface IdGenerator {
    Long nextId();
}
