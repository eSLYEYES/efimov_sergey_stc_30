// Реализовать приложение, которое вычисляет среднее арифметическое элементов массива (массив вводится с клавиатуры).

import java.util.Scanner;

class Program3 {
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Number of array elements: ");
		int n = scanner.nextInt();
		
		int array[] = new int[n];
		int arrayAverage = 0;
		//double arrayAverage = 0;

		System.out.println("Array elements: ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			arrayAverage = arrayAverage + array[i];
		}
		
		arrayAverage = arrayAverage/n;

		System.out.println("Average of array elements: " + arrayAverage);
	}
}