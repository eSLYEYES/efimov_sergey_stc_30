//Реализовать приложение, которое выполняет разворот массива (массив вводится с клавиатуры).

import java.util.Scanner;
import java.util.Arrays;

class Program2 {
	public static void main(String args[]) {

		Scanner scanner = new Scanner(System.in);
	
		System.out.print("Number of array elements: ");
		int n = scanner.nextInt();

		int array[] = new int[n];
		
		System.out.println("Array elements: ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println("Source array: " + Arrays.toString(array));

		for (int i = 0; i < array.length / 2; i++) {
			int tmpN = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = tmpN;
		}

		System.out.println("Resulting array: " + Arrays.toString(array)); // выводится массив в зеркальном представлении
	}
}