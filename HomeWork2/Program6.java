// Реализовать приложение, которое выполняет преобразование массива в число.

class Program6 {
	
	public static void main(String args[]) {
		int array[] = {4, 2, 3, 5, 7};
		int number = 0; 

		for (int i = 0; i < array.length; i++) {
			
			int mpl = 1;
			for (int j = 0; j < (array.length - i - 1); j++) {
				mpl = mpl * 10;
			}

			number = number + array[i] * mpl;
		}

		System.out.println(number); // программа должна вывести 42357
	}
}