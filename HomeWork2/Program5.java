//Реализовать приложение, которое выполняет сортировку массива методом пузырька.

import java.util.Scanner;

class Program5 {
	public static void main(String args[]) {

		Scanner scanner = new Scanner(System.in);
	
		System.out.print("Number of array elements: ");
		int n = scanner.nextInt();
		
		int array[] = new int[n];

		System.out.println("Array elements: ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		System.out.print("Source array: ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length - 1 - i; j++) {
				if (array[j] > array[j + 1]) { 
					int tmpN = array[j];
					array[j] = array[j + 1];
					array[j + 1] = tmpN;
				}
			}
			/**	
			for (int j = array.length - 1; (j - i) > 0; j--) {
				if (array[j] < array[j - 1]) { 
					int tmpN = array[j];
					array[j] = array[j - 1];
					array[j - 1] = tmpN;
				}
			}
			**/
		}

		System.out.print("Resulting array: ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}
}