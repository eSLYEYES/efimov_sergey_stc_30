// Реализовать приложение, которое выполняет преобразование массива в число.

class Program6_ {
	
	public static void main(String args[]) {

		long array[] = {0, 552, 48, 2, 33, 0, 555, 7777, 0};
		long number = 0; 
		long mpl = 1;

		number = array[array.length - 1];
		for (int i = array.length - 1; i > 0; i--) {
			long tmpN = array[i];
			while (tmpN / 10 > 0) {
				mpl *= 10;	
				tmpN /= 10;
			}
			mpl *= 10;
			number += array[i - 1] * mpl;
		}
		System.out.println("Number = " + number); 
	}
}