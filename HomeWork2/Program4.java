// Реализовать приложение, которое меняет местами максимальный и минимальный элементы массива (массив вводится с клавиатуры).

import java.util.Scanner;
import java.util.Arrays;

class Program4 {
	public static void main(String args[]) {

		Scanner scanner = new Scanner(System.in);
	
		System.out.print("Number of array elements: ");
		int n = scanner.nextInt();
		int array[] = new int[n];
		
		System.out.println("Array elements: ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println("Source array: " + Arrays.toString(array));

		int iMn = 0;
		int iMx = 0;
		
		for (int i = 0; i < array.length; i++)
		{
			if (array[i] < array[iMn]) {
				iMn = i;
			}
			if (array[i] > array[iMx]) {
				iMx = i;
			}
		}
		 
		int tmpN = array[iMx];
		array[iMx] = array[iMn];
		array[iMn]  = tmpN;


		System.out.println("Resulting array: " + Arrays.toString(array));
	}
}