//Реализовать приложение, которое выводит сумму элементов массива (массив вводится с клавиатуры).

import java.util.Scanner;

class Program1 {
	public static void main(String args[]) {

		Scanner scanner = new Scanner(System.in);
	
		System.out.print("Number of array elements: ");
		int n = scanner.nextInt();
		
		int array[] = new int[n];
		int arraySum = 0;

		System.out.println("Array elements: ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			arraySum = arraySum + array[i];
		}
		
		System.out.println("Sum of array elements: " + arraySum);
	}
}