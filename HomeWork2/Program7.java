// Реализовать приложение, которое заполняет двумерный массив M*N последовательностью
// чисел “по спирали”.
// Например, для массива 3 на 3:
// 1 2 3
// 8 9 4
// 7 6 5

import java.util.Scanner;

class Program7 {
    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Number of array lines: ");
        int x = scanner.nextInt();

        System.out.print("Number of array columns: ");
        int y = scanner.nextInt();

        int array[][] = new int[x][y];

        int minCol = 0;
        int maxCol = y - 1;

        int minRow = 0;
        int maxRow = x - 1;

        int start = 1;

        while (true) {
            for (int i = minCol; i <= maxCol; i++) {
                array[minRow][i] = start++;
            }
            minRow++;

            if (start > x * y) break;

            for (int i = minRow; i <= maxRow; i++) {
                array[i][maxCol] = start++;
            }
            maxCol--;

            if (start > x * y) break;

            for (int i = maxCol; i >= minCol; i--) {
                array[maxRow][i] = start++;
            }
            maxRow--;

            if (start > x * y) break;

            for (int i = maxRow; i >= minRow; i--) {
                array[i][minCol] = start++;
            }
            minCol++;

            if (start > x * y) break;
        }

        System.out.println("Array elements: ");
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}