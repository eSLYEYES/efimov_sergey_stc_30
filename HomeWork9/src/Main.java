import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        NumbersProcess numbersProcess1 = number -> {
            char[] arrayNumber = (Integer.toString(number)).toCharArray();
            String resultString = "";
            for (int i = arrayNumber.length - 1; i >= 0; i--) {
                resultString += arrayNumber[i];
            }
            return Integer.parseInt(resultString);
        };

        NumbersProcess numbersProcess2 = number -> {
            char[] arrayNumber = (Integer.toString(number)).toCharArray();
            String resultString = "";
            for (int i = 0; i < arrayNumber.length; i++) {
                if (Character.getNumericValue(arrayNumber[i]) != 0) {
                    resultString += arrayNumber[i];
                }
            }
            return Integer.parseInt(resultString);
        };

        NumbersProcess numbersProcess3 = number -> {
            char[] arrayNumber = (Integer.toString(number)).toCharArray();
            String resultString = "";
            for (int i = 0; i < arrayNumber.length; i++) {
                if (Character.getNumericValue(arrayNumber[i]) % 2 != 0) {
                    resultString += Character.getNumericValue(arrayNumber[i]) - 1;
                } else {
                    resultString += arrayNumber[i];
                }
            }
            return Integer.parseInt(resultString);
        };

        StringsProcess stringsProcess1 = string -> {
            char [] arrayString = string.toCharArray();
            String resultString = "";
            for (int i = arrayString.length - 1; i >= 0; i--) {
                resultString += arrayString[i];
            }
            return resultString;
        };

        StringsProcess stringsProcess2 = string -> {
            char [] arrayString = string.toCharArray();
            String resultString = "";
            for (int i = 0; i < arrayString.length; i++) {
                if (!Character.isDigit(arrayString[i])) {
                    resultString += arrayString[i];
                }
            }
            return resultString;
        };

        StringsProcess stringsProcess3 = string -> {
            char [] arrayString = string.toCharArray();
            String resultString = "";
            for (int i = 0; i < arrayString.length; i++) {
                resultString += Character.toUpperCase(arrayString[i]);
            }
            return resultString;
        };

        int sourceNumber = 250470593;
        System.out.println(numbersProcess1.process(sourceNumber));
        System.out.println(numbersProcess2.process(sourceNumber));
        System.out.println(numbersProcess3.process(sourceNumber));

        String sourceString = "Какая-то стр0ка про 38 Попугаев, 4 Удава и 15 Мартышек";
        System.out.println(stringsProcess1.process(sourceString));
        System.out.println(stringsProcess2.process(sourceString));
        System.out.println(stringsProcess3.process(sourceString));

        int[] numbers = {564, 25687, 6045, 10354, 96, 15, 203, 12904941};
        String[] srings = {"17 мгновений весны", "101 Долматинец", "Холодное лет0 53-го", "10 негритят", "Д'Артаньян и 3 мушкетёра"};

        NumbersAndStringProcessor processorNumbersAndStrings = new NumbersAndStringProcessor(numbers, srings);
        int[] resultNumbers1 = processorNumbersAndStrings.process(numbersProcess1);
        int[] resultNumbers2 = processorNumbersAndStrings.process(numbersProcess2);
        int[] resultNumbers3 = processorNumbersAndStrings.process(numbersProcess3);
        String[] resultStrings1 = processorNumbersAndStrings.process(stringsProcess1);
        String[] resultStrings2 = processorNumbersAndStrings.process(stringsProcess2);
        String[] resultStrings3 = processorNumbersAndStrings.process(stringsProcess3);
        System.out.println(Arrays.toString(resultNumbers1));
        System.out.println(Arrays.toString(resultNumbers2));
        System.out.println(Arrays.toString(resultNumbers3));
        System.out.println(Arrays.toString(resultStrings1));
        System.out.println(Arrays.toString(resultStrings2));
        System.out.println(Arrays.toString(resultStrings3));
    }
}
