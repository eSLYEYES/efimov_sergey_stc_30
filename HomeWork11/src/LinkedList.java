public class LinkedList<E> implements List<E> {

    private Node<E> first;
    private Node<E> last;

    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;
        public Node(F value) {
            this.value = value;
        }
    }

    private class LinkedListIterator implements Iterator<E> {

        private Node<E> current = first;

        @Override
        public E next() {
            Node<E> tempNode = current;
            current = current.next;
            return tempNode.value;
        }

        @Override
        public boolean hasNext() {
            return current.next != null;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;
            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        return null;
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void print() {
        Iterator<E> iterator = this.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + "->");
        }
        System.out.println(last.value);

    }
    @Override
    public void reverse() {
        Node<E> currentNode = first;
        Node<E> nextNode = first.next;
        Node<E> previousNode = null;
        Node<E> tempNode = last;
        last = first;
        while (nextNode != last) {
            previousNode = currentNode;
            currentNode = nextNode;
            if (currentNode.next != null) {
                nextNode = currentNode.next;
                currentNode.next = previousNode;
            } else {
                currentNode.next = previousNode;
                nextNode = last;
            }
        }
        last.next = null;
        first = tempNode;
    }

    @Override
    public void removeFirst(E element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }

    @Override
    public void insert(E element, int index) {
        if (index == 0) {
            Node<E> newNode = new Node<>(element);
            Node<E> current = first;
            first = newNode;
            first.next = current;
            count++;
        } else if (index == count){
            add(element);
        } else if (index > 0 && index < count && first != null) {
            int i = 0;
            Node<E> newNode = new Node<>(element);
            Node<E> current = first;
            while (i < (index - 1)) {
                current = current.next;
                i++;
            }
            newNode.next = current.next;
            current.next = newNode;
            count++;
        } else {
            System.err.println("Вышли за пределы массива");
        }

    }

    @Override
    public void removeByIndex(int index) {
        if (index == 0) {
            first = first.next;
            count--;
        } else if (index == count - 1){
            int i = 0;
            Node<E> current = first;
            while (i < (index - 1)) {
                current = current.next;
                i++;
            }
            last = current;
            last.next = null;
            count--;
        } else if (index > 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = first;
            while (i < (index - 1)) {
                current = current.next;
                i++;
            }
            current.next = current.next.next;
            count--;
        } else {
            System.err.println("Вышли за пределы массива");
        }
    }
}
