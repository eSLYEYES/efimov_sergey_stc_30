public class MainLinkedList {

    public static void main(String[] args) {
        List<String> list = new LinkedList<>();

//        list.add(777);
//        list.add(888);
//        list.add(999);
//        list.add(555);
//        list.add(33);
//        list.add(5);
//        list.add(512);
//        list.add(157);

        list.add("Hello ");
        list.add("all ");
        list.add("world! ");
        list.add("When ");
        list.add("we ");
        list.add("Dance");
        list.add("!!");
        list.add(".");
        System.out.println("size: " + list.size());
        list.print();

        list.reverse();
        list.print();

        list.removeByIndex(0);
        list.print();

        list.reverse();
        list.print();

        list.removeByIndex(6);
        list.print();
//        list.removeByIndex(3);
//        list.print();
//
        list.insert("add ",0);
        list.print();
//        list.insert(23,6);
//        list.print();
//        list.insert(77,2);
//        list.print();
//        list.insert(645,8);
//        list.print();
//        System.out.println("size: " + list.size());

        System.out.println(list.contains("we1"));
//        System.out.println(list.contains(777));
//        System.out.println(list.contains(15));
//        System.out.println(list.contains(157));

//        list.removeFirst(33);
//        list.print();
    }
}
