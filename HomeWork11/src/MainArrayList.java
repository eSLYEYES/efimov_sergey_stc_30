public class MainArrayList {

    public static void main(String[] args) {
//        List<Integer> list = new ArrayList<>();
//
//        for (int i = 0; i < 11; i++) {
//            list.add(i);
//        }
//
//        list.removeFirst(2);
//        System.out.println(list.get(8));
//        System.out.println(list.indexOf(7));
//        System.out.println("size: " + list.size());
//        System.out.println(list.contains(150));
//
//        list.insert(55, 8);
//        list.print();
//        System.out.println("size: " + list.size());
//        list.removeByIndex(6);
//        list.print();
//        list.reverse();
//        list.print();

        List<String> list = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            list.add(Integer.toString(i + (int) (Math.random() * 100)));
        }
        list.add("Hello ");
        list.add("all ");
        list.add("world! ");
        list.add("When ");
        list.add("we ");
        list.add("Dance");
        list.add("!!");

        list.print();
        list.reverse();
        list.print();

        list.removeFirst("all ");
        list.print();

        System.out.println(list.get(8));
        System.out.println(list.indexOf("we"));
        System.out.println("size: " + list.size());
        System.out.println(list.contains("Dance"));

        list.insert("Kukushka", 8);
        list.print();
        System.out.println("size: " + list.size());
        list.removeByIndex(6);
        list.print();
        list.reverse();
        list.print();
    }
}
