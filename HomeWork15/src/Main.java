import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<String> stringList = new ArrayList<>();
        try {

            BufferedReader bufferedReader = new BufferedReader(new FileReader("text.txt"));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.replaceAll("\\p{Punct}", "").toLowerCase().split(" ");
                stringList.addAll(Arrays.asList(data));
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            stringList.removeIf(string -> string.length() < 2);

            String[] stringArray = stringList.toArray(new String[0]);

            QuickSort.doSort(stringArray);
            System.out.println(Arrays.toString(stringArray));

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
