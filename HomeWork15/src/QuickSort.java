public class QuickSort {

    public static void doSort(String array[]) {
        sort(array, 0, array.length - 1);
    }

    private static void sort(String array[], int strongBorder, int pivot) {

        int leftBorder = strongBorder;

        if (pivot <= strongBorder) {
            return;
        }
        for (int current = strongBorder; current < pivot; current++) {

            if (array[current].compareTo(array[pivot]) < 0) {
                String tmp = array[current];
                array[current] = array[strongBorder];
                array[strongBorder] = tmp;
                strongBorder++;
            }
        }
        String tmp = array[strongBorder];
        array[strongBorder] = array[pivot];
        array[pivot] = tmp;
        if (strongBorder - 1 > leftBorder) {
            sort(array, leftBorder, strongBorder - 1);
        }
        if (strongBorder < pivot) {
            sort(array, strongBorder + 1, pivot);
        }
    }
}