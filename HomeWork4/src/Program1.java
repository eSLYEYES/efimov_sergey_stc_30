import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberOfElements = 0;
		do  {
			System.out.print("Please, enter number of elements of the Fibonacci sequence (0 - Exit): ");
			numberOfElements = scanner.nextInt();
			if (numberOfElements < 1) break;
			System.out.println("Answer: " + fib(numberOfElements));
		}
		while (numberOfElements > 0);
	}

	public static int fib(int numberOfElements) {
		return fib(numberOfElements, 1, 1);
	}

	public static int fib(int numberOfElements, int numberOnPreviousStep, int numberOnCurrentStep) {
		if ((numberOfElements == 1) || (numberOfElements == 2)) {
			return numberOnCurrentStep;
		}
		return fib(numberOfElements - 1, numberOnCurrentStep, numberOnPreviousStep + numberOnCurrentStep)
	}
}
