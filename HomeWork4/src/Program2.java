import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		int[] array = {12, 01, -5, 16, 88, -53, 92, -13, 2, 0, 64, -4, 781, 339, 956, 12, 88, 93, 9, 351, -182, 96, -23, 23, 10, 84, 1, 64};
		
		arrayBubbleSort (array);
		
		System.out.print("Please, enter number for search: ");
		int numberForSearch  = scanner.nextInt();
		
		if (binarySearch(array, numberForSearch) >= 0){
			System.out.println("The required number was found!");
		} else{
			System.out.println("The required number NOT found!");
		}
	}

	public static void arrayBubbleSort(int[] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = array.length - 1; (j - i) > 0; j--) {
				if (array[j] < array[j - 1]) { 
					int tmpNumber = array[j];
					array[j] = array[j - 1];
					array[j - 1] = tmpNumber;
				}
			}
		}
	}

	public static int binarySearch(int[] array, int numberForSearch) {

		if ((numberForSearch < array[0]) || (numberForSearch > array[array.length - 1])) {
			return  -1;
		}
		
		int leftPoint = 0;
		int rightPoint = array.length - 1;
		int midPoint = (rightPoint + leftPoint) / 2;
		int startIndex;

		if (array[midPoint] < numberForSearch) {
			leftPoint = midPoint + 1;
			startIndex = leftPoint;
		} else if (array[midPoint] > numberForSearch) {
			rightPoint = midPoint - 1;
			startIndex = 0;
		} else {
			return midPoint;
		}

		int sizeOfTempArray = rightPoint - leftPoint + 1;	
		int[] tempArray = new int[sizeOfTempArray];

		for (int i = 0; i < sizeOfTempArray; i++) {
			tempArray [i] = array[startIndex + i];
		}
		
		return binarySearch(tempArray, numberForSearch);
	}
}