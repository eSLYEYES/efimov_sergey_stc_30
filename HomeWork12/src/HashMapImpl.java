public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        boolean updatedEntry = false;
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else {
            MapEntry<K, V> current = entries[index];
            if (current.key.equals(key)) {
                current.value = value;
            } else {
                while (current.next != null) {
                        current = current.next;
                        if (current.key.equals(key)) {
                            current.value = value;
                            updatedEntry = true;
                        }
                }
                if (!updatedEntry) {
                    current.next = newMapEntry;
                }
            }
        }
    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            return null;
        } else {
            MapEntry<K, V> current = entries[index];
            if (current.key.equals(key)) {
                return current.value;
            } else {
                while (current.next != null) {
                    current = current.next;
                    if (current.key.equals(key)) {
                        return current.value;
                    }
                }
            }
        }
        return null;
    }
}
