public class MainSet {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();

        set.add("Марсель");
        set.add("МарселЬ");
        set.add("Иван");
        set.add("Агата");
        set.add("Дарья");
        set.add("София");
        set.add("Елена");
        set.add("Марсель");
        set.add("Филипп");
        set.add("МарселЬ");
        set.add("Филипп");
        set.add("Иван");

        System.out.println(set.contains("МарселЬ"));
        System.out.println(set.contains("Сергей"));
        System.out.println(set.contains("Марсель"));
        System.out.println(set.contains("Полина"));
        System.out.println(set.contains("София"));
        System.out.println(set.contains("Дарья"));
        System.out.println(set.contains("Павел"));
    }
}
