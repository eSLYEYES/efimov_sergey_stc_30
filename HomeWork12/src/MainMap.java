
public class MainMap {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMapImpl<>();

        map.put("Марсель", 26);
        map.put("Марсель", 27);
        map.put("Агата", 18);
        map.put("София", 18);
        map.put("Елена", 18);
        map.put("Дарья", 18);
        map.put("Регина", 18);
        map.put("Неля", 28);
        map.put("Сергей", 18);
        map.put("Иван", 18);
        map.put("Виктор Александрович", 18);
        map.put("Павел", 18);
        map.put("Филипп", 18);
        map.put("Иван", 33);
        map.put("Виктор Александрович", 33);
        map.put("Регина", 25);
        map.put("Виктор Александрович", 40);
        map.put("Регина", 18);
        map.put("Павел", 22);
        map.put("Иван", 15);

        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Денис"));
        System.out.println(map.get("Павел"));
        System.out.println(map.get("Катерина"));
        System.out.println(map.get("Полина"));
        System.out.println(map.get("Регина"));
        System.out.println(map.get("Неля"));
        System.out.println(map.get("Иван"));
        System.out.println(map.get("Виктор Александрович"));
    }
}
