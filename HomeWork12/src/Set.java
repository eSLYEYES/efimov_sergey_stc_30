public interface Set<V> {
    void add(V value);
    boolean contains(V value);
}
