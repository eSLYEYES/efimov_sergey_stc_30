public class HashSet<V> implements Set<V>{
    private static final int DEFAULT_SIZE = 16;
    private SetEntry<V> entries[] = new SetEntry[DEFAULT_SIZE];

    private static class SetEntry<V> {
        V value;
        SetEntry<V> next;

        public SetEntry(V value) {
            this.value = value;
        }
    }

    @Override
    public void add(V value) {
        SetEntry<V> newSetEntry = new SetEntry<>(value);
        int index = value.hashCode() & (entries.length - 1);
        boolean existEntry = false;
        if (entries[index] == null) {
            entries[index] = newSetEntry;
        } else {
            SetEntry<V> current = entries[index];
            if (!current.value.equals(value)) {
                while (current.next != null) {
                    current = current.next;
                    if (current.value.equals(value)) {
                        existEntry = true;
                    }
                }
                if (!existEntry) {
                    current.next = newSetEntry;
                }
            }
        }
    }

    @Override
    public boolean contains(V value) {
        int index = value.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            return false;
        } else {
            SetEntry<V> current = entries[index];
            if (current.value.equals(value)) {
                return true;
            } else {
                while (current.next != null) {
                    current = current.next;
                    if (current.value.equals(value)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
