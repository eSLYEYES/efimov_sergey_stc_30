import java.util.Scanner;

class Program3 {
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		long multipl = 1;
		int number = 1;	
		boolean isCalc = false;		
		
		while (number != 0) {
			System.out.print("Пожалуйста, введите число (0-завершение работы): ");
			number = scanner.nextInt();
			if (number != 0) {

				int sumNum = 0;
				int n = number;
				while (n > 0) {
					sumNum = sumNum + n % 10;
					n = n / 10;
				}
				
				int isPrime = 0;
				for (int i = 2; i < sumNum + 1; i++) {
					if (sumNum % i == 0) {
				        	isPrime++;
					}
				}
				if ((isPrime == 1)||(sumNum == 1)) {
					multipl = multipl * number; 
					isCalc = true;
				}
			}
		}
		if (isCalc) {
			System.out.println("Ответ: " + multipl);
		}
	}
}