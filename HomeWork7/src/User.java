public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    public static class builder {

        private User user;

        public builder() {
            user = new User();
            user.firstName = "";
            user.lastName = "";
            user.age = 0;
            user.isWorker = false;
        }

        public builder firstName(String firstName) {
            user.firstName = firstName;
            return this;
        }

        public builder lastName(String lastName) {
            user.lastName = lastName;
            return this;
        }

        public builder age (int age) {
            user.age = age;
            return this;
        }

        public builder isWorker(boolean isWorker) {
            user.isWorker = isWorker;
            return this;
        }

        public User build() {
            return user;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

}
