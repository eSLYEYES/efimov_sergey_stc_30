public class Main {
    public static void main(String[] args) {
        User user1 = new User.builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .age(100)
                .build();

        User user2 = new User.builder()
                .age(17)
                .isWorker(false)
                .lastName("Иванов")
                .build();

        User user3 = new User.builder()
                .age(55)
                .build();


        System.out.println(user2.getAge() + " " + user3.isWorker() + " " +
                user2.getFirstName() + " " + user3.getAge() + " " + user1.isWorker() + " " + user1.getAge());
    }
}
