package graphs;

public interface Graph {
    void addEdge(Edge edge);
    void printDfs(int vertex);
    void printBfs(int vertex);
    void printLee (int vertexA, int vertexB);
    void printAStar(int vertexA, int vertexB);

    void print();
}
