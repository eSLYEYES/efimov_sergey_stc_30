package graphs;

import java.util.*;

public class GraphAdjacencyListImpl implements Graph {

    private static final int MAX_VERTICES_COUNT = 10;
    private static Map<Integer, Double>[] adjecencyLists = new TreeMap[MAX_VERTICES_COUNT];
    private boolean[] visited = new boolean[MAX_VERTICES_COUNT];
    private static int [][] arrayLee = new int[MAX_VERTICES_COUNT][MAX_VERTICES_COUNT];

    public GraphAdjacencyListImpl() {
        for (int i = 0; i < adjecencyLists.length; i++) {
            adjecencyLists [i] = new TreeMap<>();
        }
    }

    public GraphAdjacencyListImpl(int a, int b) {
        Vertex [][] vertices = new VertexIntegerImpl [a][b];
        adjecencyLists = new TreeMap [a * b];
        this.visited = new boolean[a * b];
        arrayLee = new int[a * b][a * b];
        for (int i = 0; i < adjecencyLists.length; i++) {
            adjecencyLists [i] = new TreeMap<>();
        }

        int vert = 0;
        for (int i = 0; i < a; i++){
            for (int j = 0; j < b; j++){
                vertices [i][j] = new VertexIntegerImpl(vert++);
            }
        }

//      удаляем некоторые вершины
        vertices [0][3].delete(vertices [0][3].getNumber());
        vertices [1][3].delete(vertices [1][3].getNumber());
        vertices [2][3].delete(vertices [2][3].getNumber());
        vertices [2][2].delete(vertices [2][2].getNumber());
        vertices [3][3].delete(vertices [3][3].getNumber());

        for (int i = 0; i < a; i++){
            for (int j = 0; j < b; j++){
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i - 1][j - 1], Math.sqrt(2)));
                } catch (ArrayIndexOutOfBoundsException ignored) {}
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i - 1][j], 1));
                } catch (ArrayIndexOutOfBoundsException ignored) {}
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i - 1][j + 1], Math.sqrt(2)));
                } catch (ArrayIndexOutOfBoundsException ignored) {}
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i][j - 1], 1));
                } catch (ArrayIndexOutOfBoundsException ignored) {}
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i][j + 1], 1));
                } catch (ArrayIndexOutOfBoundsException ignored){}
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i + 1][j - 1], Math.sqrt(2)));
                } catch (ArrayIndexOutOfBoundsException ignored) {}
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i + 1][j], 1));
                } catch (ArrayIndexOutOfBoundsException ignored) {}
                try {
                    this.addEdge(new EdgeIntegerImpl(vertices [i][j],vertices [i + 1][j + 1], Math.sqrt(2)));
                } catch (ArrayIndexOutOfBoundsException ignored) {}
            }
        }

    }

    @Override
    public void addEdge(Edge edge) {
        if ((edge.getFirst().getNumber() >= 0) && (edge.getSecond().getNumber() >= 0)) {
            adjecencyLists[edge.getFirst().getNumber()].put(edge.getSecond().getNumber(), edge.weight());
        }
    }

    @Override
    public void printDfs(int vertex) {
        dfs(vertex);
        Arrays.fill(visited,false);
    }

    private void dfs (int vertex) {
        visited[vertex] = true;
        System.out.print(vertex + " ");
        for (Map.Entry <Integer, Double> entry : adjecencyLists[vertex].entrySet()) {
            int currentVertex = entry.getKey();
            if (!visited[currentVertex]) {
                dfs(currentVertex);
            }
        }
    }

    @Override
    public void printBfs(int vertex) {
        Deque<Integer> queue = new LinkedList<>();
        queue.add(vertex);
        visited[vertex] = true;
        while (!queue.isEmpty()) {
            int currentVertex = queue.poll();
            System.out.print(currentVertex + " ");
            for (Map.Entry <Integer, Double> entry : adjecencyLists[currentVertex].entrySet()) {
                int tmpVertex = entry.getKey();
                if (!visited[tmpVertex]) {
                    queue.add(tmpVertex);
                    visited[tmpVertex] = true;
                }
            }
        }
        Arrays.fill(visited,false);
    }

    @Override
    public void printLee(int vertexA, int vertexB) {
        Deque<Integer> queue = new LinkedList<>();
        queue.add(vertexA);
        visited[vertexA] = true;
        int wave = 0;
        int lastVertexInQueue = queue.getLast();
        while (!queue.isEmpty()) {
            int currentVertex = queue.poll();
            for (Map.Entry<Integer, Double> entry : adjecencyLists[currentVertex].entrySet()) {
                int tmpVertex = entry.getKey();
                if (!visited[tmpVertex]) {
                    queue.add(tmpVertex);
                    visited[tmpVertex] = true;
                }
                arrayLee [currentVertex][wave] = wave;
            }
            System.out.print(currentVertex + " -> ");
            if (currentVertex == vertexB) {
                break;
            }
            if (currentVertex == lastVertexInQueue) {
                wave++;
                System.out.println("\nwave: " + wave);
                lastVertexInQueue = -1;
            }
            if (lastVertexInQueue == -1) {
                if (queue.isEmpty()) {
                    System.out.println("Пути между вершинами не существует!");
                } else {
                    lastVertexInQueue = queue.getLast();
                }
            }
        }
        Arrays.fill(visited,false);
    }

    @Override
    public void printAStar(int vertexA, int vertexB) {
    }

    public void print() {
        for (int i = 0; i < adjecencyLists.length; i++) {
            System.out.println(i + " -> " + adjecencyLists [i].toString());
        }
    }
}
