package graphs;

public class VertexIntegerImpl implements Vertex {

    private int number;

    public VertexIntegerImpl(int number) {
        this.number = number;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public void delete(int vertex) {
        this.number = -1;
    }
}
