package graphs;

public interface Vertex {
    int getNumber();
    void delete (int vertex);
}
