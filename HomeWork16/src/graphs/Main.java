package graphs;

public class Main {
    public static void main(String[] args) {
//        Vertex vertex0 = new VertexIntegerImpl(0);
//        Vertex vertex1 = new VertexIntegerImpl(1);
//        Vertex vertex2 = new VertexIntegerImpl(2);
//        Vertex vertex3 = new VertexIntegerImpl(3);
//        Vertex vertex4 = new VertexIntegerImpl(4);
//        Vertex vertex5 = new VertexIntegerImpl(5);
//        Vertex vertex6 = new VertexIntegerImpl(6);
//        Vertex vertex7 = new VertexIntegerImpl(7);
//        Vertex vertex8 = new VertexIntegerImpl(8);
//        Vertex vertex9 = new VertexIntegerImpl(9);
//
//        Graph orientedGraph = new GraphAdjacencyListImpl();
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex0,vertex3, 10));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex0,vertex4, 4));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex0,vertex6, 8));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex0,vertex7, 6));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex1,vertex0, 5));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex1,vertex2, 5));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex2,vertex3, 5));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex2,vertex9, 8));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex3,vertex6, 4));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex4,vertex1, 4));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex4,vertex2, 3));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex4,vertex3, 7));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex5,vertex0, 7));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex5,vertex6, 6));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex6,vertex8, 6));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex7,vertex5, 2));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex7,vertex6, 7));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex8,vertex7, 8));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex8,vertex9, 9));
//        orientedGraph.addEdge(new EdgeIntegerImpl(vertex9,vertex3, 2));
//
//        System.out.println("AdjacencyList: ");
//        orientedGraph.print();
//
//        System.out.println("\nDFS: ");
//        orientedGraph.printDfs(0);
//
//        System.out.println("\n\nBFS: ");
//        orientedGraph.printBfs(0);
//
//        System.out.println("\n\nAlgorithm Lee: ");
//        orientedGraph.printLee(0, 9);
//        System.out.println("\n");


//        Создаём граф с ребрами между всеми существующими вершинами следующего вида:
//          0  1  2 __  4
//          5  6  7 __  9
//         10 11 __ __ 14
//         15 16 17 __ 19
//         20 21 22 23 24
        Graph orientedGraph1 = new GraphAdjacencyListImpl(5, 5);

        orientedGraph1.print();

        System.out.println("\nDFS: ");
        orientedGraph1.printDfs(0);

        System.out.println("\n\nBFS: ");
        orientedGraph1.printBfs(0);

        System.out.println("\n\nAlgorithm Lee: ");
        orientedGraph1.printLee(9, 2);

        System.out.println("\n\nAlgorithm A*: ");
        orientedGraph1.printAStar(0, 14);
    }
}
