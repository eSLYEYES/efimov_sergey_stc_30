package graphs;

public interface Edge {
    Vertex getFirst();
    Vertex getSecond();
    double weight();
}
