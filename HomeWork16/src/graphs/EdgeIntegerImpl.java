package graphs;

public class EdgeIntegerImpl implements Edge {

    private Vertex first;
    private Vertex second;
    private double weight;

    public EdgeIntegerImpl(Vertex first, Vertex second, double weight) {
        this.first = first;
        this.second = second;
        this.weight = weight;
    }

    @Override
    public Vertex getFirst() {
        return first;
    }

    @Override
    public Vertex getSecond() {
        return second;
    }

    @Override
    public double weight() {
        return weight;
    }
}
