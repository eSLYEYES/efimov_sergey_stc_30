package trees;

public interface BinarySearchTree<T extends Comparable<T>> {
    void insert(T value);
    void printDfs();
    void printDfsByStack();
    // TODO: выводить по-уровням
    void printBfs();

    // TODO:
    void remove(T value);
    boolean contains(T value);
}
