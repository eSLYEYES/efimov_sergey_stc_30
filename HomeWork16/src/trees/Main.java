package trees;

public class Main {

    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTreeImpl<>();
        tree.insert(8);
        tree.insert(3);
        tree.insert(10);
        tree.insert(1);
        tree.insert(6);
        tree.insert(14);
        tree.insert(4);
        tree.insert(7);
        tree.insert(13);

        tree.printBfs();
//        tree.printDfsByStack();
//        tree.printDfs();

        int value = 13;
        if (tree.contains(value)) {
            System.out.println("\nЗначение " + value + " есть в дереве");
        } else {
            System.out.println("\nЗначение " + value + " отсутствует в дереве");
        }

        tree.remove(6);
        tree.printBfs();
    }
}
