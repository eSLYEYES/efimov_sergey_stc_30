package trees;

import java.util.Deque;
import java.util.LinkedList;

public class BinarySearchTreeImpl<T extends Comparable<T>> implements  BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;
        int level;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        }
        else if (value.compareTo(root.value) < 0) {
            root.left = insert(root.left, value);
            root.left.level = root.level + 1;
        } else {
            root.right = insert(root.right, value);
            root.right.level = root.level + 1;
        }

        return root;
    }

    @Override
    public void printDfs() {
        dfs(root);
    }

    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);

        Node<T> current;

        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.println(current.value + " ");
            if (current.left != null) {
                stack.push(current.left);
            }
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);

        Node<T> current;
        int level = 0;

        while (!queue.isEmpty()) {
            current = queue.poll();
                if (current.level > level) {
                    System.out.println();
                    level++;
                }
            System.out.print(current.value + " ");
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }
    }

    private void dfs(Node<T> root) {
        if (root != null) {
            dfs(root.left);
            System.out.println(root.value + " ");
            dfs(root.right);
        }
    }

    @Override
    public boolean contains(T value) {
        Node<T> current = root;
        while (current != null) {
            if (current.value.compareTo(value) == 0) {
                return true;
            } else if (current.value.compareTo(value) > 0) {
                current = current.left;
            }else {
                current = current.right;
            }
        }
        return false;
    }

    @Override
    public void remove(T value) {
        removeNode(root, value);
    }

    private Node<T> removeNode(Node<T> root, T value) {
        if ((root == null) || ((root.left == null) && (root.right == null))) {
            return null;
        }

        if (root.value.compareTo(value) > 0) {
            root.left = removeNode(root.left, value);
            return root;
        } else if (root.value.compareTo(value) < 0) {
            root.right = removeNode(root.right, value);
            return root;
        }

        if (root.left == null){
            removeNode(root.right, root.right.value);
            root.right.level--;
            return root.right;
        } else if(root.right == null){
            removeNode(root.left, root.left.value);
            root.left.level--;
            return root.left;
        } else {
            Node<T> nodeWithMaxValue = findMax(root.left);
            root.value = nodeWithMaxValue.value;
            root.left = removeNode(root.left, nodeWithMaxValue.value);
            return root;
        }
    }

    private Node<T> findMax(Node<T> root) {
        if (root.right != null) {
            return findMax(root.right);
        } else {
            return root;
        }
    }
}

