public class MainArrayList {

    public static void main(String[] args) {
        List list = new ArrayList();

        for (int i = 0; i < 11; i++) {
            list.add(i);
        }

        list.removeFirst(2);
        System.out.println(list.get(8));
        System.out.println(list.indexOf(7));
        System.out.println("size: " + list.size());
        System.out.println(list.contains(150));

        list.insert(55, 8);
        list.print();
        System.out.println("size: " + list.size());
        list.removeByIndex(6);
        list.print();
        list.reverse();
        list.print();
    }
}
