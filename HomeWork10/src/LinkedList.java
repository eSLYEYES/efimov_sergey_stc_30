public class LinkedList implements List {

    private Node first;
    private Node last;

    private int count;

    private static class Node {
        int value;
        Node next;
        public Node(int value) {
            this.value = value;
        }
    }

    private class LinkedListIterator implements Iterator {

        private Node current = first;

        @Override
        public int next() {
            Node tempNode = current;
            current = current.next;
            return tempNode.value;
        }

        @Override
        public boolean hasNext() {
            return current.next != null;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;
            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        return -1;
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void print() {
        Iterator iterator = this.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + "->");
        }
        System.out.println(last.value);

    }
    @Override
    public void reverse() {
        Node currentNode = first;
        Node nextNode = first.next;
        Node previousNode = null;
        Node tempNode = last;
        last = first;
        while (nextNode != last) {
            previousNode = currentNode;
            currentNode = nextNode;
            if (currentNode.next != null) {
                nextNode = currentNode.next;
                currentNode.next = previousNode;
            } else {
                currentNode.next = previousNode;
                nextNode = last;
            }
        }
        last.next = null;
        first = tempNode;
    }

    @Override
    public void removeFirst(int element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    @Override
    public void insert(int element, int index) {
        if (index == 0) {
            Node newNode = new Node(element);
            Node current = first;
            first = newNode;
            first.next = current;
            count++;
        } else if (index == count){
            add(element);
        } else if (index > 0 && index < count && first != null) {
            int i = 0;
            Node newNode = new Node(element);
            Node current = first;
            while (i < (index - 1)) {
                current = current.next;
                i++;
            }
            newNode.next = current.next;
            current.next = newNode;
            count++;
        } else {
            System.err.println("Вышли за пределы массива");
        }

    }

    @Override
    public void removeByIndex(int index) {
        if (index == 0) {
            first = first.next;
            count--;
        } else if (index == count - 1){
            int i = 0;
            Node current = first;
            while (i < (index - 1)) {
                current = current.next;
                i++;
            }
            last = current;
            last.next = null;
            count--;
        } else if (index > 0 && index < count && first != null) {
            int i = 0;
            Node current = first;
            while (i < (index - 1)) {
                current = current.next;
                i++;
            }
            current.next = current.next.next;
            count--;
        } else {
            System.err.println("Вышли за пределы массива");
        }
    }
}
