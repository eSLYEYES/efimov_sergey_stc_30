import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        Supplier<Stream<Car>> carsStream = () -> {
            try {
                return Files.lines(Paths.get("cars.txt"))
                            .map(line -> line.split("]"))
                            .map(array -> new Car(array[0].substring(1), array[1].substring(1), array[2].substring(1),
                                    Integer.parseInt(array[3].substring(1)), Integer.parseInt(array[4].substring(1))));
            } catch (IOException e) {
                throw new IllegalArgumentException();
            }
        };

        System.out.println("1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.");
        carsStream.get().filter(car -> car.getRange() == 0 || car.getColor().equals("BLACK"))
                .forEach(car -> System.out.println(car.getNumber()));

        System.out.println("\n2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");
        System.out.println(carsStream.get().filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .map(Car::getModel).distinct().count());

        System.out.println("\n3) Цвет автомобиля с минимальной стоимостью.");
        System.out.println(carsStream.get().min(Comparator.comparingInt(Car::getPrice)).get().getColor());

        System.out.println("\n4) Средняя стоимость Camry.");
        System.out.printf("%.2f", carsStream.get().filter(car -> car.getModel().equals("Toyota Camry"))
                .mapToInt(Car::getPrice).average().getAsDouble());

    }
}
