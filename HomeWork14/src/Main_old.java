import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main_old {
    public static void main(String[] args) {
        try {
            System.out.println("1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.");
            Stream<Car> carsStream = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split("]"))
                    .map(array -> new Car(array[0].substring(1), array[1].substring(1), array[2].substring(1),
                            Integer.parseInt(array[3].substring(1)), Integer.parseInt(array[4].substring(1))))
                    .filter(car -> car.getRange() == 0 || car.getColor().equals("BLACK"));
            carsStream.forEach(car -> System.out.println(car.getNumber()));

            System.out.println("\n2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");
            carsStream = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split("]"))
                    .map(array -> new Car(array[0].substring(1), array[1].substring(1), array[2].substring(1),
                            Integer.parseInt(array[3].substring(1)), Integer.parseInt(array[4].substring(1))))
                    .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000);
            Stream<String> uniqueCarsBetween700and800 = carsStream.map(Car::getModel).distinct();
            System.out.println(uniqueCarsBetween700and800.count());

            System.out.println("\n3) Цвет автомобиля с минимальной стоимостью.");
            carsStream = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split("]"))
                    .map(array -> new Car(array[0].substring(1), array[1].substring(1), array[2].substring(1),
                            Integer.parseInt(array[3].substring(1)), Integer.parseInt(array[4].substring(1))));
            System.out.println(carsStream.min(Comparator.comparingInt(Car::getPrice)).get().getColor());

            System.out.println("\n4) Средняя стоимость Camry.");
            carsStream = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split("]"))
                    .map(array -> new Car(array[0].substring(1), array[1].substring(1), array[2].substring(1),
                            Integer.parseInt(array[3].substring(1)), Integer.parseInt(array[4].substring(1))))
                    .filter(car -> car.getModel().equals("Toyota Camry"));
            IntStream priceCamry = carsStream.mapToInt(Car::getPrice);
            System.out.printf("%.2f", priceCamry.average().getAsDouble());

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
