import java.util.Objects;

public class Car {
    private String number;
    private String model;
    private String color;
    private Integer range;
    private Integer price;

    public Car(String number, String model, String color, Integer range, Integer price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.range = range;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getRange() {
        return range;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(number, car.number) &&
                Objects.equals(model, car.model) &&
                Objects.equals(color, car.color) &&
                Objects.equals(range, car.range) &&
                Objects.equals(price, car.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, range, price);
    }

    @Override
    public String toString() {
        return "Car{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", range=" + range +
                ", price=" + price +
                '}';
    }
}
