public class Channel {
    private int number;
    private String name;
    private String genre;
    private int numbersOfProgram;
    private Program[] programs;

    public Channel(int number, String name, String genre) {
        this.number = number;
        this.name = name;
        this.genre = genre;
        this.programs = new Program[5];
    }

    public String getName() {
        return name;
    }

    public String getGenre() {
        return genre;
    }

    public void takeProgram(Program program) {
        this.programs[numbersOfProgram] = program;
        this.numbersOfProgram++;
    }

    public void Show() {
        int i = (int) (Math.random () * 5); //случаное число от 0 до 4
        System.out.println("Channel " + this.getName() + "(" + this.getGenre() + ") showing:");
        System.out.println("    " + this.programs[i].getTimeStart() + " " +this.programs[i].getName());
    }
}
