public class TeleVision {
    private String Brand;
    private String Model;
    private RemoteController remoteController;
    private int numbersOfChannel; //текущий номер настроенного канала
    private Channel[] channels; // количетво каналов в ТВ

    public TeleVision(String brand, String model) { //конструктор
        this.Brand = brand;
        this.Model = model;
        this.channels = new Channel[50];
    }

    public void pairRemoteController(RemoteController remoteController) {
        this.remoteController = remoteController;
        this.remoteController.setTeleVision(this);
    }

    public void tuneTeleVision(Channel channel) {
        this.channels[numbersOfChannel] = channel;
        this.numbersOfChannel++;
    }

    public void powerOnTeleVision () {
        System.out.println(this.Brand + " " + this.Model + " POWER ON!");
    }

    public void powerOffTeleVision () {
        System.out.println("\n" + this.Brand + " " + this.Model + " POWER OFF...");
    }

    public void changeChannel(int channel) {
        System.out.println("\nSwitch to channel " + channel + "...");
        this.channels[channel - 1].Show();
    }

}
