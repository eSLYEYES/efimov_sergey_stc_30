public class RemoteController {
    private TeleVision teleVision;

    public void setTeleVision(TeleVision teleVision) {
        this.teleVision = teleVision;
    }

    public void changeChannel(int channel) {
        this.teleVision.changeChannel(channel);
    }
}
