import java.util.Date;

public class Program {
    private String name;
    private String timeStart;
    private Channel channel;

    public Program(String timeStart, String name) {
        this.timeStart = timeStart;
        this.name = name;
    }

    public void bindToChannel (Channel channel) {
        this.channel = channel;
        this.channel.takeProgram(this);
    }

    public String getName() {
        return name;
    }

    public String getTimeStart() {
        return timeStart;
    }
}
