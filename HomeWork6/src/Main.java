public class Main {
    public static void main(String[] args) {
        TeleVision television = new TeleVision("Gnusmas","TV55S");
        RemoteController remoteController = new RemoteController();

        Channel ch1 = new Channel(1,"TV1000", "Cinema");
        Channel ch2 = new Channel(2,"Discovery", "Informative");
        Channel ch3 = new Channel(3, "1TV", "General");

        Program pr1 = new Program("05:00", "Форсаж. Х/ф");
        Program pr2 = new Program("06:45", "Двойной форсаж. Х/ф");
        Program pr3 = new Program("08:30", "Тройной форсаж: Токийский дрифт. Х/ф");
        Program pr4 = new Program("14:25", "Форсаж-6. Х/ф");
        Program pr5 = new Program("16:45", "Остров головорезов. Х/ф");
        Program pr6 = new Program("09:40", "Охотники за складами");
        Program pr7 = new Program("10:06", "Братья Дизель");
        Program pr8 = new Program("11:23", "Странные связи");
        Program pr9 = new Program("11:48", "Взрывая историю");
        Program pr10 = new Program("13:30", "Как это устроено?. Д/с");
        Program pr11 = new Program("09:30", "Утро России");
        Program pr12 = new Program("09:55", "О самом главном");
        Program pr13 = new Program("11:00", "Вести");
        Program pr14 = new Program("11:30", "Судьба человека с Борисом Корчевниковым");
        Program pr15 = new Program("12:40", "60 минут");

        television.powerOnTeleVision();

        television.pairRemoteController(remoteController);

        television.tuneTeleVision(ch1);
        television.tuneTeleVision(ch2);
        television.tuneTeleVision(ch3);

        pr1.bindToChannel(ch1);
        pr2.bindToChannel(ch1);
        pr3.bindToChannel(ch1);
        pr4.bindToChannel(ch1);
        pr5.bindToChannel(ch1);

        pr6.bindToChannel(ch2);
        pr7.bindToChannel(ch2);
        pr8.bindToChannel(ch2);
        pr9.bindToChannel(ch2);
        pr10.bindToChannel(ch2);

        pr11.bindToChannel(ch3);
        pr12.bindToChannel(ch3);
        pr13.bindToChannel(ch3);
        pr14.bindToChannel(ch3);
        pr15.bindToChannel(ch3);

        remoteController.changeChannel(2);
        remoteController.changeChannel(1);
        remoteController.changeChannel(3);
        remoteController.changeChannel(2);
        remoteController.changeChannel(3);
        remoteController.changeChannel(3);

        television.powerOffTeleVision();
    }
}
