//	Реализовать приложение, со следующим набором функций и процедур:
//	● выводит сумму элементов массива
//	● выполняет разворот массива (массив вводится с клавиатуры).
//	● вычисляет среднее арифметическое элементов массива (массив вводится с клавиатуры).
//	● меняет местами максимальный и минимальный элементы массива
//	● выполняет сортировку массива методом пузырька.
//	● выполняет преобразование массива в число.

import java.util.Scanner;
import java.util.Arrays;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Please, enter number of array elements: ");
		int size = scanner.nextInt();
		
		int array[] = new int[size];
		
		System.out.println("Array elements (please, >= 0): ");
		for (int i = 0; i < array.length; i++) {
			System.out.print("[" + i + "] = ");
			array[i] = scanner.nextInt();
		}
		
		System.out.println("\nSum of array elements: " + ArraySum(array));
		
		ArrayReverse(array);
		System.out.println("\nArray after reverse: " + Arrays.toString(array)); 
		
		System.out.printf("\nAverage of array elements: %.3f\n", ArrayAverage(array));
		
		ArrayMinMaxSwap (array);
		System.out.println("\nArray after swap Min and Max elements: " + Arrays.toString(array)); 
		
		ArrayBubbleSort(array);
		System.out.println("\nArray after bubble sort: " + Arrays.toString(array)); 
		
		System.out.println("\nTransformation array to number: " + ArrayToNumber(array));
	}

	//возвращает сумму элементов массива
	public static int ArraySum(int array []) {
		int arraySum = 0;
		for (int i = 0; i < array.length; i++) {
			arraySum += array[i];
		}
		return arraySum;
	}

	//выполняет разворот массива
	public static void ArrayReverse(int array []) {
		for (int i = 0; i < array.length / 2; i++) {
			int tmpN = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = tmpN;
		}
	}

	//возвращает среднее арифметическое элементов массива
	public static double ArrayAverage(int array []) {
		double arrayAverage = 0;
		for (int i = 0; i < array.length; i++) {
			arrayAverage += array[i];
		}
		return arrayAverage/array.length;
	}

	//меняет местами максимальный и минимальный элементы массива
	public static void ArrayMinMaxSwap(int array []) {
		int iMin = 0;
		int iMax = 0;
		for (int i = 0; i < array.length; i++)
		{
			if (array[i] < array[iMin]) {
				iMin = i;
			}
			if (array[i] > array[iMax]) {
				iMax = i;
			}
		}
		int tmpN = array[iMax];
		array[iMax] = array[iMin];
		array[iMin]  = tmpN;
	}

	//сортировка массива методом пузырька
	public static void ArrayBubbleSort(int array []) {
		for (int i = 0; i < array.length; i++) {
			for (int j = array.length - 1; (j - i) > 0; j--) {
				if (array[j] < array[j - 1]) { 
					int tmpN = array[j];
					array[j] = array[j - 1];
					array[j - 1] = tmpN;
				}
			}
		}
	}

	//возвращает значение преобразования элементов массива в число
	public static long ArrayToNumber(int array []) {
		long number = array[array.length - 1];;
		long mpl = 1;
		for (int i = array.length - 1; i > 0; i--) {
			long tmpN = array[i];
			while (tmpN / 10 > 0) {
				mpl *= 10;
				tmpN /= 10;
			}
			mpl *= 10;
			number += array[i - 1] * mpl;
		}
		return number;
	}
}