//	Реализовать приложение, рассчитывающее определенный интеграл методом Симпсона
//	Рассмотреть случаи для разных разбиений.

class Program2 {
	public static void main(String[] args) {
		int ns[] = {10, 100, 1_000, 10_000, 100_000, 1_000_000};
		printIntegralResultsForN(0, 10, ns);
	}

	public static double f(double x) {
		return (x / (x * x * (x - 2) + 3));
	}

	public static double integralBySimpson(double a, double b, int n) {
		double h = (b - a) / (2 * n);
		double result = 0;
		for (double x = a + h; x <= b - h; x += (h * 2)) {
			result += h/3 * (f(x - h) + 4 * f(x) + f(x + h));
		}
		return result;
	}

	public static void printIntegralResultsForN(double a, double b, int ns[]) {
		for (int i = 0; i < ns.length; i++) {
			System.out.println("For N = " + ns[i] + ", result = " + integralBySimpson(a, b, ns[i]));
		}
	}

}