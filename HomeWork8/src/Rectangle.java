public class Rectangle extends Figure {
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
        this.coordinateX = (int) Math.ceil(width / 2);
        this.coordinateY = (int) Math.ceil(length / 2);
    }
    @Override
    public double getPerimeter () {
        return 2 * (length + width);
    }

    @Override
    public double getAreas () {
        return length * width;
    }

    @Override
    public void scale(double scale) {
        this.length *= scale;
        this.width *= scale;
        if (length == width) {
            System.out.println("New edge: " + this.length +
                    "\nNew areas: " + this.getAreas() + ", new perimeter " + this.getPerimeter());
        } else {
            System.out.println("New length: " + this.length + ", new width: " + this.width +
                    "\nNew areas: " + this.getAreas() + ", new perimeter " + this.getPerimeter());
        }
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }
}

