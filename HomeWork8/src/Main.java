/**
 * Date: 10.04.2023
 * HomeWork8
 *
 * @author Efimov Sergey
 * @version v1.0
 */

public class Main {

    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(15, 20);
        System.out.println("Ellipse (r: " + ellipse.getSmallRadius()+ ", R: " + ellipse.getLargeRadius() + "):\nAreas: " +
                ellipse.getAreas() + ", perimeter:  " + ellipse.getPerimeter() + ", X: " +
                ellipse.coordinateX + ", Y: " + ellipse.coordinateY);
        ellipse.replace(100, -6);
        ellipse.scale(1.5);

        Ellipse circle = new Circle(7);
        System.out.println("\nCircle (r: " + circle.getSmallRadius() + "):\nAreas: " +
                circle.getAreas() + ", perimeter:  " + circle.getPerimeter() + ", X: " +
                circle.coordinateX + ", Y: " + circle.coordinateY);
        circle.replace( 200, 150);
        circle.scale(0.7);
        circle.replace( -100, -50);
        circle.scale(2.1);

        Rectangle rectangle = new Rectangle(7,18);
        System.out.println("\nRectangle (length: " + rectangle.getLength() + ", width: " + rectangle.getWidth() + "):\nAreas: " +
                rectangle.getAreas() + ", perimeter:  " + rectangle.getPerimeter() + ", X: " +
                rectangle.coordinateX + ", Y: " + rectangle.coordinateY);
        rectangle.replace(100, -6);
        rectangle.scale(1.5);

        Rectangle square = new Square(10);
        System.out.println("\nSquare (edge: " + square.getLength() + "):\nAreas: " +
                square.getAreas() + ", perimeter:  " + square.getPerimeter() + ", X: " +
                square.coordinateX + ", Y: " + square.coordinateY);
        square.replace(115, 256);
        square.scale(0.6);
        square.replace(65, -120 );
        square.scale(2);
    }
}