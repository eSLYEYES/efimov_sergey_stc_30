public interface FigureScale {
    void scale(double scale);
}
