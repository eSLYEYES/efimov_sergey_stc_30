import static java.lang.Math.*;

public class Ellipse extends Figure {
    private double smallRadius;
    private double largeRadius;

    public Ellipse(double smallRadius, double largeRadius) {
        this.smallRadius = smallRadius;
        this.largeRadius = largeRadius;
        this.coordinateX = (int) largeRadius;
        this.coordinateY = (int) smallRadius;
    }

    @Override
    public double getPerimeter () {
        return 2 * PI * sqrt((pow(smallRadius, 2) + pow(largeRadius, 2))/2);
    }

    @Override
    public double getAreas () {
        return PI * smallRadius * largeRadius;
    }

    @Override
    public void scale(double scale) {
        this.smallRadius *= scale;
        this.largeRadius *= scale;
        if (largeRadius == smallRadius) {
            System.out.println("New radius: " + this.smallRadius +
                    "\nNew areas: " + this.getAreas() + ", new perimeter " + this.getPerimeter());
        } else {
            System.out.println("New small radius: " + this.smallRadius + ", new large radius: " + this.largeRadius +
                    "\nNew areas: " + this.getAreas() + ", new perimeter " + this.getPerimeter());
        }
    }

    public double getSmallRadius() {
        return smallRadius;
    }

    public double getLargeRadius() {
        return largeRadius;
    }

}
