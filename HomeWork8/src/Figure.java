public abstract class Figure implements FigureReplace, FigureScale {
    protected int coordinateX;
    protected int coordinateY;

    public abstract double getPerimeter();
    public abstract double getAreas();
    public abstract void scale(double scale);

    public void replace(int x, int y) {
        this.coordinateX += x;
        this.coordinateY += y;
        System.out.println("New coordinate X: " + this.coordinateX + ", Y:" + this.coordinateY);
    }
}
